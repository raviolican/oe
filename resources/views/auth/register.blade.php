@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-md-2 col-md-offset-3"></div>
                <h1 style="margin-top: 0px; margin-bottom: 20px">Account Erstellen</h1>
                <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Pseodonym</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-Mail Addresse</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Passwort</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password-confirm" class="col-md-4 control-label">Passwort bestätigen</label>

                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                    </div>

                    <hr>

                    <div class="form-group">
                        <label for="firstName" class="col-md-4 control-label">Vorname</label>

                        <div class="col-md-6">
                            <input id="firstName" type="text" class="form-control" name="firstName" value="{{ old('firstName') }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="lastName" class="col-md-4 control-label">Nachname</label>

                        <div class="col-md-6">
                            <input id="lastName" type="text" class="form-control" name="lastName" value="{{ old('lastName') }}" required autofocus>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address" class="col-md-4 control-label">Straße, nummer</label>

                        <div class="col-md-6">
                            <input id="address" type="text" class="form-control" name="address" value="{{ old('adress') }}" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="plz" class="col-md-4 control-label">PLZ</label>

                        <div class="col-md-6">
                            <input id="plz" type="number" class="form-control" name="plz" value="{{ old('adress') }}" required autofocus >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="city" class="col-md-4 control-label">Ort</label>

                        <div class="col-md-6">
                            <input id="city" type="text" class="form-control" name="city" value="{{ old('adress') }}" required autofocus >
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="bnd" class="col-md-4 control-label">Bundesland</label>

                        <div class="col-md-6">
                            <select  class="form-control" name="bnd" id="bnd" required autofocus>
                                <option value=""></option>
                                <option value="vo">Vorarlberg</option>
                                <option value="ti">Tirol</option>
                                <option value="sa">Salsburg</option>
                                <option value="st">Steiermark</option>
                                <option value="oo">Oberösterreich</option>
                                <option value="no">Niederösterreich</option>
                                <option value="bu">Burgenland</option>
                                <option value="wi">Wien</option>
                                <option value="ka">Kärnten</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Registrieren
                            </button>
                            <br>
                        </div>

                    </div>

                </form>

            </div>
        </div>
    </div>
</div>

@endsection
