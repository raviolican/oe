<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        @if (isset($thread))
            {{ $thread->title }} -
        @endif
        @if (isset($category))
            {{ $category->title }} -
        @endif
        {{ trans('forum::general.home_title') }}
    </title>
    <script src="{{ asset('js/app.js') }}"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>


    textarea {
        min-height: 200px;
    }

    .deleted {
        opacity: 0.65;
    }


    .navbar a {
        color: white !important;
    }
    body {
        background-color: white !important;
    }
    * {
        border-radius: 0 !important;
    }
    .navBarLeft {
        margin-left: 20px;

    }
    .navBarLeft:after {
        content: '\a';
        white-space: pre;
    }
    .navBarLeft.header {
        font-weight: bold;

        margin-left: 15px;
    }
    </style>
</head>
<body>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-101412167-1', 'auto');
    ga('send', 'pageview');

</script>
<div class="container">
    <br>
    <span style="background-color: #1b6d85 !important;">
            <span style="float: left">
                <img src="{{asset("images/oecoin-logo.png")}}" alt="" style="height: 30px;">
                <b>CRYPTO TECHNOL.</b> <BR> <i>Virtuelle Währung in <u>Österreich</u></i>
            </span>
            <span style="float: right">
                <h1 style="margin-top: 0px;margin-left: 25px"></h1>
                <ul style="list-style: none">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Registration</a></li>
                    @else
                        Willkommen,
                        {{ Auth::user()->name }}!
                        <br>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                        <a href="{{route("accounthome")}}">Account</a>
                        </li>
                    @endif
                </ul>
            </span>
        </span>
    <br>
    <br>
    <br>

    <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Branding Image -->
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{url("/")}}">Home</a>
                    </li>
                    <li><a href="{{route("Blog")}}">Blog</a></li>
                    <li><a href="#">Handeln</a></li>
                    <li><a href="{{url("/forum")}}">Forums</a></li>
                </ul>

                <!-- Right Side Of Navbar -->

            </div>
        </div>
    </nav>
        @include ('forum::partials.breadcrumbs')
        @include ('forum::partials.alerts')

        @yield('content')


    <script>
    var toggle = $('input[type=checkbox][data-toggle-all]');
    var checkboxes = $('table tbody input[type=checkbox]');
    var actions = $('[data-actions]');
    var forms = $('[data-actions-form]');
    var confirmString = "{{ trans('forum::general.generic_confirm') }}";

    function setToggleStates() {
        checkboxes.prop('checked', toggle.is(':checked')).change();
    }

    function setSelectionStates() {
        checkboxes.each(function() {
            var tr = $(this).parents('tr');

            $(this).is(':checked') ? tr.addClass('active') : tr.removeClass('active');

            checkboxes.filter(':checked').length ? $('[data-bulk-actions]').removeClass('hidden') : $('[data-bulk-actions]').addClass('hidden');
        });
    }

    function setActionStates() {
        forms.each(function() {
            var form = $(this);
            var method = form.find('input[name=_method]');
            var selected = form.find('select[name=action] option:selected');
            var depends = form.find('[data-depends]');

            selected.each(function() {
                if ($(this).attr('data-method')) {
                    method.val($(this).data('method'));
                } else {
                    method.val('patch');
                }
            });

            depends.each(function() {
                (selected.val() == $(this).data('depends')) ? $(this).removeClass('hidden') : $(this).addClass('hidden');
            });
        });
    }

    setToggleStates();
    setSelectionStates();
    setActionStates();

    toggle.click(setToggleStates);
    checkboxes.change(setSelectionStates);
    actions.change(setActionStates);

    forms.submit(function() {
        var action = $(this).find('[data-actions]').find(':selected');

        if (action.is('[data-confirm]')) {
            return confirm(confirmString);
        }

        return true;
    });

    $('form[data-confirm]').submit(function() {
        return confirm(confirmString);
    });
    </script>

    @yield('footer')
    <span style="text-align: justify;float: left">
            <img src="{{asset("images/oecoin-logo.png")}}" alt="" style="height: 20px;">
        </span> <br>
</body>
</html>
