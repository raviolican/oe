<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ÖCOIN</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style type="text/css">
        .navbar a {
            color: white !important;
        }
        body {
            background-color: white !important;
        }
        * {
            border-radius: 0 !important;
        }
        .navBarLeft {
            margin-left: 20px;

        }
        .navBarLeft:after {
            content: '\a';
            white-space: pre;
        }
        .navBarLeft.header {
            font-weight: bold;

            margin-left: 15px;
        }

    </style>
</head>
<body>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-101412167-1', 'auto');
    ga('send', 'pageview');

</script>
    <div class="container">
        <br>
        <span style="background-color: #1b6d85 !important;">
            <span style="float: left">
                <img src="{{asset("images/oecoin-logo.png")}}" alt="" style="height: 30px;">
                <b>CRYPTO TECHNOL.</b> <BR> <i>Virtuelle Währung in <u>Österreich</u></i>
            </span>
            <span style="float: right">
                <h1 style="margin-top: 0px;margin-left: 25px"></h1>
                <ul style="list-style: none">
                    <!-- Authentication Links -->
                     @if (Auth::guest())
                         <li><a href="{{ route('login') }}">Login</a></li>
                         <li><a href="{{ route('register') }}">Registration</a></li>
                     @else
                                Willkommen,
                                    {{ Auth::user()->name }}!
                        <br>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                                <a href="{{route("accounthome")}}">Account</a>
                            </li>
                     @endif
                </ul>
            </span>
        </span>
        <br>
        <br>
        <br>

        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Branding Image -->
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="{{url("/")}}">Home</a>
                        </li>
                        <li><a href="{{route("Blog")}}">Blog</a></li>
                        <li><a href="#">Handeln</a></li>
                        <li><a href="{{url("/forum")}}">Forums</a></li>
                    </ul>

                    <!-- Right Side Of Navbar -->

                </div>
            </div>
        </nav>

        @if (isset($errors) &&count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @yield('content')

        <span style="text-align: justify;float: left">
            <img src="{{asset("images/oecoin-logo.png")}}" alt="" style="height: 20px;">
        </span> <br>

    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    @yield('jScript')
</body>
</html>

