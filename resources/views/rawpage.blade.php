@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 " style="margin-left: 15px;">
            <h1>{{$site->siteName}}</h1>
            {!! $site->siteContent !!}
        </div>
    </div>
@endsection
