
<img src="{{asset("images/oecoin-logo.png")}}" alt="" style="height: 30px;"> <br>
Guten Tag {{$user}},
<br><br>
Es wurde nachstehende Transaktion eingeleitet: <br>
<table>
    <tr>
        <th>Empfänger</th>
        <th>Betrag </th>
    </tr>
    <tr>
        <td>{{$to}}</td>
        <td>{{$amount}} ÖCO</td>
    </tr>
</table>
<br>
Klicken Sie den Link um diese Transaktion zu bestätigen.
<br>
<a href="{{route("verifyTX", $id)}}">{{route("verifyTX", $id)}}</a>
<br><br><br>
Grüße,
ÖCOIN.AT