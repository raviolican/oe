@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route("accounthome")}}">Kontrollzentrum</a></li>
        <li class="breadcrumb-item active">Wallet</li>
    </ol>
    <div class="col-md-12" style="padding:0px;">

        <div class="col-md-2 col-md-offset-0" style="padding: 0px !important;" >
            @include('usercp.menu')
            <br>

        </div>

        <div class="col-md-10 ">
            <h3>Neue Transaktion</h3>
            <h4>
                Verfügbar: {{ ($walletArray["balance"])}} ÖCO

            </h4>
            <form action="" method="post">
                {{csrf_field()}}
                <div class="form-group{{ $errors->has('recipient') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-2 control-label">Empfänger</label>
                    <div class="col-md-10">
                        <input id="recipient"  placeholder="Empfänger" class="form-control" name="to" value="{{ old('email') }}" required>
                    </div>
                </div>
                <br> <br>
                <div class="form-group{{ $errors->has('recipient') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-2 control-label">Betrag</label>
                    <div class="col-md-10">
                        <input id="recipient" type="number" placeholder="0.0" class="form-control" name="amount" value="{{ old('email') }}" required>
                    </div>
                </div>
                <br> <br>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Bestätigen
                        </button>
                        <br>
                    </div>

                </div>
            </form>
            <br><br>
            <p>
                Bestätige die eMail um die Transaktion auszuführen.
            </p>
        </div>
    </div>
@endsection
@section("jScript")

@endsection