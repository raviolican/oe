@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route("accounthome")}}">Kontrollzentrum</a></li>
        <li class="breadcrumb-item active">Wallet</li>
    </ol>
    <div class="col-md-12" style="padding:0px;">

        <div class="col-md-2 col-md-offset-0" style="padding: 0px !important;" >
            @include('usercp.menu')
            <br>

        </div>

        <div class="col-md-10 ">
            <h3>ÖCOIN Wallet</h3>
            <h4>
                Balance: {{ (($walletArray["balance"])[0])}} ÖCO
                @if(isset($walletArray["balance"][1]))
                    Locked: {{$walletArray["balance"][1]}} ÖCO
                @endif
            </h4>
            Deine Addresse: <br><code>{{$walletArray["address"]}}
            </code>
            <br>Verwende diese Addresse um ÖCO zu erhalten. Transaktionen dauern zZ. bis zu 5 Minuten.
            <br> <br>

            <a href="{{route("rendernewtx")}}">
                <button type="button" class="btn btn-md btn-info">
                    Neue Transaktion
                </button>
            </a>
            <br>
            <h3>Transaktionen</h3>
            <table class="table">
                <small>
                @foreach($walletArray["history"] as $tx)
                    <tr>
                        <td>Txhash: {{$tx->txhash}} <br>
                            Amount: <b>{{hexdec($tx->amount)}} ÖCO </b><br>
                            Recipient: {{$tx->to}}
                        </td>
                    </tr>
                @endforeach
                </small>
            </table>
        </div>
    </div>
@endsection
@section("jScript")

@endsection