@extends('layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route("accounthome")}}">Kontrollzentrum</a></li>
        <li class="breadcrumb-item active">Wallet</li>
    </ol>
    <div class="col-md-12" style="padding:0px;">

        <div class="col-md-2 col-md-offset-0" style="padding: 0px !important;" >
            @include('usercp.menu')
            <br>

        </div>

        <div class="col-md-10 ">
            <h3>Wallet</h3>
            <p style="border-left: 3px solid #31b0d5; padding-left:5px;">
                Erzeugen Sie hier ein Wallet. Auf dieses werden die Coins des Faucets vergünstigt. <br>
                Sobald sie genügend Coins haben, können Sie diese auszahlen lassen. <br> <br>
                Wir versuchen die maximale Sicherheit für Ihre coins. <br> <br>
                > ETHER TESTNET ROPSTEN
            </p>
            <br><br>
            <input type="text" id="passphrase" placeholder="Passphrase"> <br> <br>

            <button type="button" class="btn btn-lg btn-success" id="genwallet">Wallet Erzeugen</button>
            <br><br><br>
        </div>
    </div>
@endsection
@section("jScript")
    <script>
        $(document).ready(function () {
            $("#genwallet").on('click', function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    type: "POST",
                    url: "{{route("genwallet")}}",
                    data: "?k={{md5(\Illuminate\Support\Facades\Auth::user()->name)}}",
                    success:function(response) {

                    }
                });
            })
        })
    </script>
@endsection