@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 " style="margin-left: 15px;">
            <p>
                <h1>Transaktions Übersicht </h1>

            <hr>
            <code>Txhash: {{$info["hash"]}} <br>
                Amount: {{$info["amount"]}} ÖCO <br>
                Sender: {{$info["sender"]}} <br>
                Recipient: {{$info["recipient"]}}
            </code>

            <hr>
            <h3>Status</h3>
            {{$status}}
            </p>


        </div>
    </div>
@endsection
