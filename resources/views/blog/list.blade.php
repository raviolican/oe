@extends('layouts.app')

@section('content')
    <div class="col-md-12" style="padding:0px;">
        <div class="col-md-2 col-md-offset-0" style="padding: 0px !important;" >
            @include('admin.menu')
            <br>

        </div>
        <div class="col-md-10 ">
            <p>
            <h1>
                BLOG
            </h1>
            </p>

                <!-- Default panel contents -->

                <!-- Table -->
                <table class="table">
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Tools</th>
                    </tr>
                    @foreach($postings as $article)
                        <tr>
                            <td>{{($article->id)}}</td>
                            <td>{{($article->title)}}</td>
                            <td>
                                <form action="{{route("deletePost", $article->id)}}" method="post" style="float:left;">
                                    {{csrf_field()}}
                                    <button type="submit" class="btn btn-xs btn-primary">Löschen</button>
                                </form>

                                <a href="{{route("editBlogPost",$article->id)}}">
                                    <button type="button" class="btn btn-xs btn-primary">Bearbeiten</button>
                                </a>

                                
                        </tr>
                    @endforeach
                </table>



        </div>
    </div>
@endsection
