@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 ">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route("home")}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{route("Blog")}}">Blog</a></li>
                <li class="breadcrumb-item active">{{$post->title}}</li>
            </ol>
            <div class="panel-body">
                    <div class="media">
                        <div class="media-left">
                                <img class="media-object"
                                     src="{{asset("images/blog_images/".$post->imageFile)}}"
                                     alt="blgobild" width="70px">

                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">{!! $post->title!!}</h4>
                            <i style="font-size: x-small"> {{$post->created_at->format("d-m-y") }}

                                <br></i>
                            <p>
                                <i> {{$post->description}}</i>
                            </p>
                            <p>
                                {!!$post->body!!}
                            </p> <br><br>
                            <div id="disqus_thread"></div>
                            <script>

                                /**
                                 *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                                 *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                                /*
                                 var disqus_config = function () {
                                 this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
                                 this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                                 };
                                 */
                                (function() { // DON'T EDIT BELOW THIS LINE
                                    var d = document, s = d.createElement('script');
                                    s.src = 'https://ocoin-at-1.disqus.com/embed.js';
                                    s.setAttribute('data-timestamp', +new Date());
                                    (d.head || d.body).appendChild(s);
                                })();
                            </script>
                            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

                        </div>
                    </div>
                <p>
                </p>
            </div>
        </div>
    </div>
@endsection
