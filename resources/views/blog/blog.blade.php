@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-10 ">
            <div class="panel-body">
                <h1></h1>

                @foreach($posts as $post)
                    <div class="media">
                        <div class="media-left">
                            <a href="{{route("viewblog",[$post->slug,$post->id])}}">
                                <img class="media-object"
                                     src="{{asset("images/blog_images/".$post->imageFile)}}"
                                     alt="blgobild" width="70px">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">{!! $post->title!!}</h4>
                            <i style="font-size: x-small"> {{$post->created_at->format("d-m-y") }}

                                <br></i>
                            <p>
                                {{$post->description}}
                            </p>
                        </div>
                    </div>
                @endforeach
                @if(count($posts) <= 0)
                    Keine Einträge gefunden.
                @endif
            </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>
@endsection
