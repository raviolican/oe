@extends('layouts.app')

@section('content')
    <div class="col-md-12" style="padding:0px;">
        <div class="col-md-2 col-md-offset-0" style="padding: 0px !important;" >
            @include('admin.menu')

        </div>
        <div class="col-md-10 ">
            <p>
            <h3>
                {{$post->title}}
            </h3>
            <p style="border-left: 3px solid #31b0d5; padding-left:5px;">Blubb</p>
            </p>
            <form id="generalSettingsForm" action="{{route("saveEditPost",$post->id)}} " method="post"
                  class="form-horizontal" role="form">

                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title" class="col-md-3 control-label">Titel</label>
                    <div class="col-md-9">
                        <input id="title" type="text" value="{{$post->title}}" class="form-control"
                               name="title" required autofocus>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                    <label for="slug" class="col-md-3 control-label">SEO Slug</label>
                    <div class="col-md-9">
                        <input id="slug" type="text" class="form-control"
                               name="slug" placeholder="Slug" value="{{$post->slug}}" required autofocus>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('keywords') ? ' has-error' : '' }}">
                    <label for="keywords" class="col-md-3 control-label">Schlagwörter</label>
                    <div class="col-md-9">
                        <input id="keywords"  value="{{$post->keywords}}"  type="text" class="form-control"
                               name="keywords" required autofocus>
                    </div>
                </div>

                <!--AUTHOR-->
                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description" class="col-md-3 control-label">Beschreibung</label>
                    <div class="col-md-9">
                        <textarea id="description" type="text" class="form-control"
                                  name="description" placeholder="HTML/JS erlaubt" required autofocus >
                            {{$post->description}}
                        </textarea>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                    <label for="body " class="col-md-3 control-label">Text</label>
                    <div class="col-md-9">
                        <textarea id="body " type="text"  class="form-control"
                                  name="body" placeholder="HTML/JS erlaubt" required autofocus >
                           {{$post->body}}
                        </textarea>
                    </div>
                </div>



                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary" id="submit">
                            Erstellen
                        </button>
                        <span id="response" hidden></span>
                        <br>
                    </div>


                </div>
            </form>
        </div>
    </div>


@endsection
@section('jScript')

@endsection
