@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">


                <div class="panel-body">
                    <p>
                        Willkommen auf {{$settings["siteName"]}}, dem Österreichischem Portal für <br>
                        Virtuelle Währungen.
                    </p>
                    {{$settings["siteName"]}}
                </div>
            </div>
        </div>
    </div>
@endsection
