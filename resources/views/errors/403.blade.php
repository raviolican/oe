@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-md-12 col-md-offset-0" style="margin-left: 0px;">

            <div class="panel panel-danger">
                <div class="panel-heading">Fehler</div>
                <div class="panel-body">
                    <p style="font-family: Consolas">
                        HTTP-Error: 403 <br>
                        Zugriff verweigert. <br>
                    </p>

                </div>
            </div>
        </div>
    </div>
@endsection
