@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 ">

            <h1>ÖCOIN Virtuelle Währung Österreich</h1>
            <h2>Was ist Virtuelle Währung?</h2>

            <blockquote>
                <p>
                    Cryptocurrency ist eine <i>Virtuelle Währung</i> welche Kryptographie verwendet um
                    Geld zu erzeugen und Transaktionen zu verifizieren. Transaktionen werden in eine Art
                    "Ledger" (eine art "Buch") gespeichert, auch bezeichnet als <i>block</i>. Neue <i>coins</i>
                    wrden durch <i>mining</i> (dt. "Schürfen") generiert.
                </p>
            </blockquote>
            <hr>


        </div>
    </div>
@endsection
