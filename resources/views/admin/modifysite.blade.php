@extends('layouts.app')

@section('content')
    <div class="col-md-12" style="padding:0px;">
        <div class="col-md-2 col-md-offset-0" style="padding: 0px !important;" >
            @include('admin.menu')
            <br>

        </div>

        <div class="col-md-10 ">
            @if(Session::has("msg"))
                <div class="alert alert-info">{{ Session::get('msg') }}</div>
            @endif
            <p>
            <h1>
                "{{$site->siteName}}"
            </h1>
            </p>
            <? // action="{{route('newsite')}}" ?>
            <form id="generalSettingsForm"   method="post" class="form-horizontal" role="form">
                {{ csrf_field() }}
                <input type="hidden" name="action" value="patch">
                <div class="form-group{{ $errors->has('siteName') ? ' has-error' : '' }}">
                    <label for="siteName" class="col-md-3 control-label">Titel</label>
                    <div class="col-md-9">
                        <input id="siteName" type="text" class="form-control"
                               name="siteName" value="{{$site->siteName}}" required autofocus>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('siteName') ? ' has-error' : '' }}">
                    <label for="siteURLSlug" class="col-md-3 control-label">SEO Slug</label>
                    <div class="col-md-9">
                        <input id="siteName" type="text" class="form-control"
                               name="siteURLSlug" value="{{$site->siteURLSlug}}" required autofocus>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('siteDescription') ? ' has-error' : '' }}">
                    <label for="siteDescription" class="col-md-3 control-label">Beschreibung</label>
                    <div class="col-md-9">
                        <input id="siteDescription" type="text" class="form-control"
                               name="siteDescription" value="{{$site->siteDescription}}" required autofocus>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('siteDescription') ? ' has-error' : '' }}">
                    <label for="siteContent " class="col-md-3 control-label">Inhalt</label>
                    <div class="col-md-9">
                        <textarea id="siteContent " type="text" class="form-control"
                                  name="siteContent"    required autofocus >{{$site->siteContent}}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="minUserGroup " class="col-md-3 control-label">Benutzergruppe >= </label>
                    <div class="col-md-9">
                        <select name="minUserGroup" id="minUserGroup ">
                            <option value="0" {{ $site->minUserGroup == 0 ? "selected" : "" }}>Admin</option>
                            <option value="1" {{ $site->minUserGroup == 1 ? "selected" : "" }}>Manager</option>
                            <option value="2" {{ $site->minUserGroup == 2 ? "selected" : "" }}>Blogger</option>
                            <option value="3" {{ $site->minUserGroup == 3 ? "selected" : "" }}>Registrierte Benutzer</option>
                            <option value="4" {{ $site->minUserGroup == 4 ? "selected" : "" }}>Jeder</option>
                        </select>
                        <span style="margin-left: 10px;font-size: smaller">
                            Bestimmen Sie, ab welche Benutzergruppe die Seite besucht werden darf.
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="siteActive " class="col-md-3 control-label">Aktiv</label>
                    <div class="col-md-9">
                        <select name="siteActive" id="siteActive ">
                            <option value="1" {{ $site->siteActive == 1 ? "selected" : "" }}>Ja</option>
                            <option value="0" {{ $site->siteActive == 0 ? "selected" : "" }}>Nein</option>
                        </select>
                        <span style="margin-left: 10px;font-size: smaller">
                            Aktivieren, wenn die Seite sofort erreichbar sein soll.
                        </span>
                    </div>
                </div>



                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary" id="submit">
                            Speichern
                        </button>
                        <span id="response" hidden></span>
                        <br>
                    </div>

                </div>
            </form>

            <form id="remove"   method="post" class="form-horizontal" role="form">
                {{ csrf_field() }}
                <input type="hidden" name="action" value="delete">
                <div class="form-group">
                    <div class="col-md-12 col-md-offset-0">
                        <button style="float: right" type="submit" class="btn btn-danger" id="submit">
                            Entfernen
                        </button>
                        <br>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection
