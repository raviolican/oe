@extends('layouts.app')

@section('content')
    <div class="col-md-12" style="padding:0px;">
        <div class="col-md-2 col-md-offset-0" style="padding: 0px !important;" >
            @include('admin.menu')

        </div>
        <div class="col-md-10 ">
            <p>
            <h3>
                Seite ertellen
            </h3>
            <p style="border-left: 3px solid #31b0d5; padding-left:5px;">Erstellen Sie eine neue Seite</p>

            </p>
            <form id="generalSettingsForm" action="{{route('saveGeneral')}}" class="form-horizontal" role="form">
                <div class="form-group{{ $errors->has('siteName') ? ' has-error' : '' }}">
                    <label for="siteName" class="col-md-3 control-label">Websiten Name</label>
                    <div class="col-md-9">
                        <input id="siteName" type="text" class="form-control"
                               name="siteName" value="{{$settings["siteName"]}}" required autofocus>
                    </div>
                </div>
                <div class="form-group{{ $errors->has('siteDescription') ? ' has-error' : '' }}">
                    <label for="siteDescription" class="col-md-3 control-label">Beschreibung</label>
                    <div class="col-md-9">
                        <input id="siteDescription" type="text" class="form-control"
                               name="siteDescription" value="{{$settings["siteDescription"]}}" required autofocus>
                    </div>
                </div>
                <div class="form-group">
                    <label for="offlineMode" class="col-md-3 control-label">Entwickler Modus</label>
                    <div class="col-md-9">
                        <select name="offlineMode" id="offlineMode">
                            <option value="1" {{ $settings["offlineMode"] ? "selected" : "" }}>An</option>
                            <option value="0" {{ $settings["offlineMode"] ? "selected" : "" }}>Aus</option>
                        </select>
                        <span style="margin-left: 10px;font-size: smaller">
                            Aktiviert den Entiwckler Modus mit spezeillen Zutritts-Regeln.
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="allowBlogComments" class="col-md-3 control-label">Blog Kommentare</label>
                    <div class="col-md-9">
                        <select name="allowBlogComments" id="allowBlogComments">
                            <option value="1" {{ $settings["allowBlogComments"] ? "selected" : "" }}>An</option>
                            <option value="0" {{ $settings["allowBlogComments"] ? "selected" : "" }}>Aus</option>
                        </select>
                        <span style="margin-left: 10px;font-size: smaller">
                            Besucher können nach deaktivierung nichtmehr kommentieren
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="allowRegistrations" class="col-md-3 control-label">Registrierung deaktivieren</label>
                    <div class="col-md-9">
                        <select name="allowRegistrations" id="allowRegistrations">
                            <option value="1" {{ $settings["allowRegistrations"] ? "selected" : "" }}>An</option>
                            <option value="0" {{ $settings["allowRegistrations"] ? "selected" : "" }}>Aus</option>
                        </select>
                        <span style="margin-left: 10px;font-size: smaller">
                            De-/Aktiviert die Möglichkeit dass sich neue Benutzer registrieren dürfen.
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="button" class="btn btn-primary" id="submit">
                            Speichern
                        </button>
                        <span id="response" hidden></span>
                        <br>
                    </div>


                </div>
            </form>
        </div>
    </div>


@endsection
@section('jScript')
    <script>
        $(document).ready(function () {
            var cfgForm = $('#generalSettingsForm');
            $('#submit').click(function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "PATCH",
                    url: cfgForm.attr('action'),
                    data: cfgForm.serialize(),
                    success :function(response) {
                        $('#response').fadeIn(500).text("Gespeichert!").fadeOut(500);
                    }
                });
            });
        });
    </script>
@endsection
