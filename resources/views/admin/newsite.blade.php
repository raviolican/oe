@extends('layouts.app')

@section('content')
    <div class="col-md-12" style="padding:0px;">
        <div class="col-md-2 col-md-offset-0" style="padding: 0px !important;" >
            @include('admin.menu')

        </div>
        <div class="col-md-10 ">
            <p>
            <h3>
                Seite ertellen
            </h3>
            <p style="border-left: 3px solid #31b0d5; padding-left:5px;">Erstellen Sie eine neue Seite</p>

            </p>
            <form id="generalSettingsForm" action="{{route('newsite')}}" method="post" class="form-horizontal" role="form">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('siteName') ? ' has-error' : '' }}">
                    <label for="siteName" class="col-md-3 control-label">Titel</label>
                    <div class="col-md-9">
                        <input id="siteName" type="text" class="form-control"
                               name="siteName" placeholder="Meine neue Seite" required autofocus>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('siteName') ? ' has-error' : '' }}">
                    <label for="siteURLSlug" class="col-md-3 control-label">SEO Slug</label>
                    <div class="col-md-9">
                        <input id="siteName" type="text" class="form-control"
                               name="siteURLSlug" placeholder="Slug" required autofocus>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('siteDescription') ? ' has-error' : '' }}">
                    <label for="siteDescription" class="col-md-3 control-label">Beschreibung</label>
                    <div class="col-md-9">
                        <input id="siteDescription" type="text" class="form-control"
                               name="siteDescription" placeholder="Kurze Beschreibung" required autofocus>
                    </div>
                </div>

                <div class="form-group{{ $errors->has('siteDescription') ? ' has-error' : '' }}">
                    <label for="siteContent " class="col-md-3 control-label">Inhalt</label>
                    <div class="col-md-9">
                        <textarea id="siteContent " type="text" class="form-control"
                                  name="siteContent" placeholder="HTML/JS erlaubt" required autofocus ></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="minUserGroup " class="col-md-3 control-label">Benutzergruppe >= </label>
                    <div class="col-md-9">
                        <select name="minUserGroup" id="minUserGroup ">
                            <option value="0" >Admin</option>
                            <option value="1" >Manager</option>
                            <option value="2" >Blogger</option>
                            <option value="3" >Registrierte Benutzer</option>
                            <option value="4" >Jeder</option>
                        </select>
                        <span style="margin-left: 10px;font-size: smaller">
                            Bestimmen Sie, ab welche Benutzergruppe die Seite besucht werden darf.
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="siteActive " class="col-md-3 control-label">Aktiv</label>
                    <div class="col-md-9">
                        <select name="siteActive" id="siteActive ">
                            <option value="1">Ja</option>
                            <option value="0">Nein</option>
                        </select>
                        <span style="margin-left: 10px;font-size: smaller">
                            Aktivieren, wenn die Seite sofort erreichbar sein soll.
                        </span>
                    </div>
                </div>



                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary" id="submit">
                            Erstellen
                        </button>
                        <span id="response" hidden></span>
                        <br>
                    </div>


                </div>
            </form>
        </div>
    </div>


@endsection
@section('jScript')

@endsection
