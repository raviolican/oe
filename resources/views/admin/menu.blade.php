<div class="list-group" style="margin: 0px !important;padding:0px !important;">
    <span class="navBarLeft header">General</span>
    <a href="{{route('adminStats')}}" class="navBarLeft">Statistiken</a>
    <a href="{{route('adminGeneral')}}" class="navBarLeft">Grundlegendes</a>
    <a href="#" class="navBarLeft">API Keys</a>
    <a href="#" class="navBarLeft">Werbung</a>
    <span class="navBarLeft header">Eigene Seiten</span>

    @foreach(\App\Page::all() as $k)
        <a href="{{url("admin/editsite/".$k->id)}}" class="navBarLeft">{{$k->siteName}}</a>
    @endforeach
    <a href="{{route('newsite')}}" class="navBarLeft">> Neu <</a>
    <span class="navBarLeft header">Blog</span>
    <a href="#" class="navBarLeft">Blong Einstellungen</a>
    <a href="{{route("renderNewPost")}}" class="navBarLeft">Beitrag erstellen</a>
    <a href="{{route('listArticles')}}" class="navBarLeft">Beiträge anzeigen</a>
    <br>
    <a href="#" class="navBarLeft">Cach Leeren</a>
</div>
