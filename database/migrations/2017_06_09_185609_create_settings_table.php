<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key')->prim;
            $table->string('value')->nullable();
            $table->timestamps();
        });

        DB::table('settings')->insert(
            array(
                ["key" => "siteName", "value" => "ÖCOIN"],
                ["key" => "siteDescription", "value" => "Crypto Währung Österreich"],
                ["key" => "offlineMode", "value" => 0],
                ["key" => "allowRegistrations", "value" => 1],
                ["key" => "allowBlogPosts", "value" => 1],
                ["key" => "allowAdSense", "value" => 1],
                ["key" => "allowGoogle", "value" => 1],
                ["key" => "googleAnalyticsKey", "value" => NULL],
                ["key" => "disqusAPIKey", "value" => NULL],
                ["key" => "allowBlogComments", "value" => 1],
                ["key" => "blogPostsPerPage", "value" => 10],




            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
