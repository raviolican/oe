<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', function () {
    return view('welcome');
});




Route::group(['middleware' => 'auth'], function () {
    Route::group(['middleware' => 'admin'], function () {
        Route::get('/admin', 'Administration\AdministrationController@renderIndex')->name('adminIndex');
        Route::group(['prefix' => 'admin'], function () {
            Route::get('general', 'Administration\AdministrationController@renderGeneral')->name('adminGeneral');
            Route::get('stats', 'Administration\AdministrationController@renderStats')->name('adminStats');

            Route::get('newsite', 'Administration\AdministrationController@renderNewSite')->name('newsite');
            Route::post('newsite', 'Administration\AdministrationController@postNewSite')->name('newsite');

            Route::get('editsite/{id}', 'Administration\AdministrationController@editSite');
            Route::post('editsite/{id}', 'Administration\AdministrationController@patchSite');


            Route::patch('general', 'Administration\AdministrationController@saveGeneral')->name('saveGeneral');
        });

    });
    Route::group(['prefix' => 'blog'], function () {

        Route::get('newarticle', 'Blog\BlogController@renderNewPost')->name('renderNewPost');
        Route::post('newarticle', 'Blog\BlogController@newBlogPost')->name('newBlogPost');

        Route::get('list', 'Blog\BlogController@getUserBlogArticles')->name('listArticles');
        Route::get('editpost/{id}', 'Blog\BlogController@editBlogPost')->name('editBlogPost');
        Route::post('delPost/{id}', 'Blog\BlogController@deleteBlogPost')->name('deletePost');
        Route::post('saveEditPost/{id}', 'Blog\BlogController@saveEditedPost')->name('saveEditPost');
    });
    Route::group(['prefix' => 'account'], function () {
        Route::get('/', 'UserController@cphome')->name('accounthome');

        Route::group(['prefix' => 'wallet'], function () {
            Route::get('/', 'UserController@wallethome')->name('accountwallet');
            Route::post('/', 'UserController@genwallet')->name('genwallet');
            Route::get('/transaction', 'UserController@rendernewtx')->name('rendernewtx');
            Route::post('/transaction', 'UserController@newtx')->name('newtx');
        });


    });
    Route::get("validate/{key}/", "UserController@validateTransaction")->name("verifyTX");




});
Route::group(['prefix' => 'blog'], function () {
    Route::get('/', 'Blog\BlogIndexViewController@renderIndex')->name('Blog');
    Route::get('/view/{slug}/{id}','Blog\BlogIndexViewController@getBlogPosts')->name('viewblog');

});
Route::get('test', 'EthController@test');

Route::group(['prefix' => 'faucet'], function () {
    Route::get('/', function () {
        return view('faucets.home');
    });

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

#Route::get('{slug}/{id}/', 'CustomPageController@render');