<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = array(
        "amount","txhash","activatekey", "isactive", "aborted", "status","to"
    );
    public function user() {
        return $this->belongsTo('App\User');
    }
}
