<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = array(
        "siteName", "siteContent", "siteDescription", "siteURLSlug",
        "siteActive", "minUserGroup"
    );
}
