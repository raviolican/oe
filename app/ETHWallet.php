<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ETHWallet extends Model
{
    public function owner() {
        $this->belongsTo('App\User');
    }
}
