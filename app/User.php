<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Support\Facades\Auth;


/*
 * GROUPS:
 * 0 - > ADMIN
 * 1 - > MANAGER
 * 2 - > EMPLOYEE
 * 3 - > NORMAL USERS
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', "address", "realName", "bundesLand", "group"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function blogArticles() {
        if($this->isEmployee()) {
            return $this->hasMany("\App\BlogArticle");
        } else {
            return false;
        }
    }

    /*
     * ETHEREUM Wallet
     */
    public function ETHWallet() {
        return $this->hasOne('App\ETHWallet');
    }

    public function Transactions() {
        return $this->hasMany('App\Transaction');
    }
    /*
     * Check is the user is in a group that can actually post Blog-Articles
     */
    public function isEmployee() {
        return ($this->group <= 2);
    }

    /*
     * Check if user is in manager group
     */
    public function isManager()  {
        return ($this->group <=1);
    }

    /*
     * Check if user is admin
     */
    public function isAdmin() {
        return ($this->group == 0);
    }
    public function isAllowedBlogger() {
        return (Auth::user()->group <= 2) ? true:false;
    }
    public function wallet() {
        return $this->hasMany("App\Wallet");
    }
}
