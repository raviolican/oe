<?php

namespace App\Http;
use App\Http\Controllers\ShoppingCartController;
use App\Http\Controllers\ShoppingController;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;
use App\Settings;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 *
 */
class SettingsComposer
{
    public function compose(View $view){
        // Settings
        $view->with("settings", \Cache::remember('settings', 10, function() {
            //return Settings::all()->keyBy('key')->toArray();
            return Settings::pluck('value','key');
        }));




    }

}