<?php

namespace App\Http\Controllers;
require_once ('Crypto/Currencies/WalletFactory.php');
use App\ETHWallet;
use App\Transaction;
use App\User;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController extends Controller
{
    public function __construct()
    {

    }

    public function cphome() {
        return view('usercp.account');
    }
    public function wallethome() {
        if(Auth::user()->wallet()->get()->all() != NULL) {
            $obj = new \WalletFactory("oecoin");
            $obj = $obj->getObject();

            $balance = $obj->getBalance("oecoin");
            $wallet = Auth::user()->wallet()->where("coin_id","=", "oecoin")->get()->first();
            $walletArray = array(
                "address" => json_decode($wallet->args)->address,
                "balance" => json_decode($balance,true),
                "history" => Auth::user()->Transactions()->paginate(100)
            );
            return view("usercp.wallet", compact("walletArray"));
        } else { // No wallet created yet
            return view("usercp.mkwallet");
        }

        /*
        $obj = new \WalletFactory("oecoin");
        $obj = $obj->getObject();
        */


    }
    /*
     * Void func to create wallet
     */
    public function genwallet(Request $request)
    {
        $var = null;
        if(($var = Auth::user()->wallet()->where("coin_id","=", "oecoin")->get()->first()) != null) {
            return;
        }

        $obj = new \WalletFactory("oecoin");
        $obj = $obj->getObject();
        $obj->createNewWallet();
        // Todo Check if user not allready created wallet X



        /*
        dd(Auth::user()->ETHWallet);
        if(Auth::user()->ETHWallet == NULL) {
            $sock = socket_create(AF_UNIX, SOCK_STREAM, 0);
            if($sock == FALSE) {
                return FALSE;
            }
            if(!socket_connect($sock, "/home/sappy/.ethereum/geth.ipc",1)) {
                return FALSE;
            }
            // Block Ressource
            if(!socket_set_block($sock)) {
                return FALSE;
            }

            $myBuf = null;
            $msg = "{\"method\":\"personal_newAccount\",\"params\":[\"TST\"],\"id\":1}";
            if(!socket_send($sock, $msg, strlen($msg), MSG_EOF)) {
                return FALSE;
            }
            #sleep(5);
            if(!socket_recv ( $sock , $myBuf ,  100 ,MSG_WAITFORONE)) {
                return FALSE;
            }
            if(!socket_set_nonblock($sock)) {
                // HANDLE ERROR
            }
            if(!socket_close($sock)) {
                // HANDLE ERROR
            }
            // Success
            $wallet = new ETHWallet();
            $wallet->address = json_decode($myBuf)->result;
            $usr = User::find(Auth::user()->id);
            return $usr->ETHWallet()->save($wallet);
        } else {
            return "wallet_exists";
        }
        */
    }
    private function geth_sock($query) {
        if(Auth::user()->ETHWallet != NULL) {
            $sock = socket_create(AF_UNIX, SOCK_STREAM, 0);
            if($sock == FALSE) {
                return FALSE;
            }
            if(!socket_connect($sock, "/home/sappy/.ethereum/geth.ipc",1)) {
                return FALSE;
            }
            // Block Ressource
            if(!socket_set_block($sock)) {
                return FALSE;
            }

            $myBuf = null;
            if(!socket_send($sock, $query, strlen($query), MSG_EOF)) {
                return FALSE;
            }
            #sleep(5);
            if(!socket_recv ( $sock , $myBuf ,  150 ,MSG_WAITFORONE)) {
                return FALSE;
            }
            if(!socket_set_nonblock($sock)) {
                // TODO HANDLE ERROR
            }
            if(!socket_close($sock)) {
                // TODO HANDLE ERROR
            }
            // Success
            return $myBuf;
        } else {
            return false;
        }
    }
    public function rendernewtx(Request $request) {

        if(Auth::user()->wallet()->get() != NULL) {
            $obj = new \WalletFactory("oecoin");
            $obj = $obj->getObject();
            $balance = $obj->getBalance("oecoin");
            $wallet = Auth::user()->wallet()->where("coin_id","=", "oecoin")->get()->first();
            $walletArray = array(
                "address" => json_decode($wallet->args)->address,
                "balance" => json_decode($balance,true)[0],
            );
            return view("usercp.newtx", compact("walletArray"));
        } else { // No wallet created yet
            return view("usercp.mkwallet");
        }
    }
    public function newtx(Request $request)
    {
        $this->validate($request, array(
            "amount" => "numeric|required",
            "to"    => "required|min:80|max:100"

        ));
        $key = (string)random_int(rand(-1,-200000),rand(1,200000));

        $tx = new Transaction(array(
            "txhash" => NULL,
            "amount" => dechex($request->amount),
            "activatekey" => $key,
            "isactive" => 1,
            "aborted" => false,
            "status" => 0,
            "to" => $request->to
        ));
        $user = Auth::user();
        $user->Transactions()->save($tx);

        /* generate key */
        $t = array(
            "id" => $tx->id,
            "key"=> $key

        );
        $t = encrypt($t);
        // Implement E-Mail!
        Mail::send('mail.validateTransaction',
            ['user' => decrypt($user->realName), 'id'=>$t,
                "amount" => $request->amount, "to" => $request->to],
        function($m) use ($user) {
            $m->from('secure@oecoin.at', "ÖCOIN Transaktionen");
            $m->to("daumsimon94@gmail.com")->subject("ÖCOIN Transaktionen");
        });
        $request->session()->flash("msg", "Eine Bestätigungs E-Mail wurde versendet.");

    }
    public function validateTransaction($id) {
        try{
            $john = decrypt($id);
        } catch(DecryptException $e) {
            throw new NotFoundHttpException();
        }

        $tx = Auth::user()->Transactions->find($john["id"]);

        if($tx == NULL)
            throw new NotFoundHttpException();
        if(!($tx->activatekey == $john["key"] ))
            throw new NotFoundHttpException();
        /* Deprecated?
        $transaction = array(
            "from" => Auth::user()->wallet()->where("coin_id","=","OECOIN")->get()->first()->address,
            "to" => $tx->to,
            "value" => "0x".$tx->amount
        );
        */
        $wallet = Auth::user()->wallet()->where("coin_id","=","OECOIN")->get()->first();
        $wallet_args = json_encode($wallet->args);
        $tx->txhash         = "NaN"; /* Todo: Specify */
        $tx->status         = 1;
        $tx->activatekey    = 0;
        $tx->save();

        $obj = new \WalletFactory("oecoin");
        $obj = $obj->getObject();
        $result = $obj->transfer($tx->to,$tx->amount);

        if($result) {
            $tx->txhash = $result;
            $tx->status = 2;

            $status = "Server meldet erfolg.";

        } else {
            $tx->txhash = "Could not send transaction.";
            $status = "Server meldet, dass die Transaktion nicht gesendet werden konnte.";
        }
        $tx->save();

        $info = array(
            "hash" => $tx->txhash,
            "amount"=>$tx->amount,
            "sender"=>$wallet_args->address,
            "recipient"=>$tx->to
        );
        return  view("txverify", compact("info","status"));

    }
}
/*
 * 0 Inititated
 * 1 Activated
 * 2 Sending
 * 3 Sent
 */