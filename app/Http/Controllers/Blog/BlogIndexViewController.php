<?php

namespace App\Http\Controllers\Blog;

use App\BlogArticle;
use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use phpDocumentor\Reflection\DocBlock\Tags\SinceTest;
use phpDocumentor\Reflection\Types\Null_;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BlogIndexViewController extends Controller
{
    public function __construct()
    {
       # if(Cache::get("settings")["allowRegistrations"] == 0)
        #    throw new AccessDeniedHttpException();

    }

    public function renderIndex()
    {
        $posts = BlogArticle::all();

        return view('blog.blog', compact("posts"));

    }
    public function getBlogPosts(Request $request,$slug, $id)
    {
        $post = BlogArticle::find($id);
        if($post == NULL) {
            throw new NotFoundHttpException();
        } else {
            return view("blog.viewarticle", compact("post"));
        }

    }
}
