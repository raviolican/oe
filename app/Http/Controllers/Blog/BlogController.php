<?php

namespace App\Http\Controllers\Blog;

use App\BlogArticle;
use App\Page;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\DocBlock\Tags\SinceTest;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class BlogController extends Controller
{
    public function renderNewPost() {

        /*
         * TEST
         */


        if(Auth::user()->isAllowedBlogger())
            return view('blog.newpost');#
        throw new AccessDeniedHttpException();
    }

    public static function hashName($str) {
        $myHash = md5($str);
        $myHash = hex2bin($myHash);
        $key = decbin(random_int(1,2000));
        $myHash = $myHash^$key;
        return md5($myHash);


    }
    public function newBlogPost(Request $request) {
        $usr = Auth::user();
        if(!Auth::check() && !$usr->isAllwoedBlogger())
            throw new AccessDeniedHttpException();
        $this->validate($request, array(
            "title" => "required",
            "slug" => "required",
            "keywords" => "required",
            "body" => "required",
        ));

        $newPost = new BlogArticle($request->except("_token"));

        $fileName =  BlogController::hashName("hello").".".$request->file("datei")
                ->getClientOriginalExtension();
        $request->file("datei")->move(
            base_path().'/public/images/blog_images/',
           $fileName
        );

        #$newPost->imageFile = $request->datei->save("blog_images");
        $newPost->imageFile = $fileName;
        $newPost->author = $usr->name;
        if( $newPost->save()) {
            $request->session()->flash("msg", "Beitrag veröffentlicht");
            return redirect()->back();
        }
    }
    public function getUserBlogArticles() {
        if(Auth::user()->isAdmin())
            $postings = BlogArticle::all();
        else if(Auth::user()->isAllowedBlogger())
            $postings = BlogArticle::where('author', Auth::user()->name);
        else
            throw new AccessDeniedHttpException();
        return view("blog.list", compact("postings"));
    }
    public function editBlogPost(Request $request, $id)
    {
        $post = BlogArticle::find($id);
        if($post != null)
            return view("blog.editpost", compact("post"));
        else
            return redirect()->back()->withErrors("Post nicht gefunden-");
    }
    public function deleteBlogPost(Request $request, $id) {
        if(BlogArticle::destroy($id)) {
            $request->session()->flash("msg", "Beitrag gelöscht!");
            return redirect()->back();
        } else {
            return redirect()->back()
                ->withErrors("Beitrag konnte nicht gelöscht werden.");
        }
    }
    public function saveEditedPost(Request $request,$id) {
        $post = BlogArticle::find($id);
        if($post == NULL)
            return redirect()->back()->withErrors("Beitrag nicht gefunden.");
        $post->title = $request->title;
        $post->slug = $request->slug;
        $post->keywords = $request->keywords;
        $post->body = $request->body;
        $post->description = $request->description;
        if($post->save()) {
            $request->session()->flash("msg", "Beitrag erfolgreich gespeichert");
            return redirect()->back();
        } else {
            return redirect()->back()
                ->withErrors("Beitrag konnte nicht gespeichert werden");
        }
    }
}
