<?php

namespace App\Http\Controllers;

use App\Page;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CustomPageController extends Controller
{
    public function render($slug,$id)
    {
        $site = Page::find($id);

        // Check if site exists
        if ($site == null)
            throw new NotFoundHttpException();

        // Check if site is Active
        if ($site->siteActive != 1)
            throw new AccessDeniedHttpException();

        // Check if user group fits requirements
        if ($site->minUserGroup < 4) {
            if (Auth::check()) { // Must be logged in
                if (!(Auth::user()->group <= $site->minUserGroup)) {
                    throw new AccessDeniedHttpException();
                }
            } else {
                throw new AccessDeniedHttpException();
            }
        }

        return view('rawpage', compact('site'));

    }
}
