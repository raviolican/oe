<?php

namespace App\Http\Controllers\Administration;

use App\Page;
use App\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use phpDocumentor\Reflection\Types\Null_;
use phpDocumentor\Reflection\Types\Object_;

class AdministrationController extends Controller
{
    private $request;
    public function renderIndex()
    {
        return view('admin.index');
    }
    public function renderGeneral()
    {
        return view('admin.general');
    }
    public function renderStats()
    {
        return view('admin.stats');
    }
    public function renderNewSite()
    {
        return view('admin.newsite');
    }
    public function postNewSite(Request $request)
    {
        $this->validate($request, [
            "siteName" => 'required|max:200',
            "siteContent" => 'required',
            "siteDescription" => 'required|max:200',
            "siteURLSlug" => 'required',
            "siteActive" => 'required|numeric',
            "minUserGroup" => 'required',
        ]);

        $site = new Page($request->except("_token"));
        $site->save();

        return view('admin.newsite');
    }
    public function editSite($id) {
        $site = Page::find($id);
        return view("admin.modifysite", compact("site"));
    }
    public function patchSite(Request $request, $id) {
        if($request->action == "patch") {

            $this->validate($request, [
                "siteName" => 'required|max:200',
                "siteContent" => 'required',
                "siteDescription" => 'required|max:200',
                "siteURLSlug" => 'required',
                "siteActive" => 'required|numeric',
                "minUserGroup" => 'required',
            ]);
            $site = Page::find($id);
            $site->siteName = $request->siteName;
            $site->siteContent = $request->siteContent;
            $site->siteDescription = $request->siteDescription;
            $site->siteURLSlug = $request->siteURLSlug;
            $site->siteActive = $request->siteActive;
            $site->minUserGroup = $request->minUserGroup;
            if ($site->save()) {
                $request->session()->flash("msg", "Seite gespeichert.");
                return redirect()->back();
            }
        } else if($request->action == "delete") {
            if(Page::destroy($id)) {
                $request->session()->flash("msg", "Seite gelöscht.");
                return redirect(route("adminIndex"));
            } else {
                return redirect()->back()->withErrors("Seite konnte nicht gelöscht werden.");
            }
        }

    }
    public function saveGeneral(Request $request) {
        $this->validate($request, [
           "siteName" => 'required|max:200',
           "siteDescription" => 'required|max:200',
            "offlineMode" => 'required',
            "allowBlogComments" => 'required',
        ]);

        foreach($request->all() as $k => $v) {
            Settings::where("key", $k)->update(["value" => $v]);

        }
        Cache::forget("settings");

    }
}
