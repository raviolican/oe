<?php
/**
 * Created by PhpStorm.
 * User: sappy
 * Date: 20.06.17
 * Time: 16:35
 */

class JSONRPCHandler {
    private $url;
    private $port;
    private $curlObject;
    public function __construct($url,$port)
    {
        if(is_string($url) && url != NULL) {
            $this->url = $url;
        }
        if(is_integer($port) && $port != 0) {
            $this->port = $port;
        }
        // Init Curl Object
        $this->curlObject = curl_init($this->url+":"+(string)$this->port);
        if(!$this->curlObject) {
            // Todo: handle errors
        }
    }
    public function exec($request) {
        curl_setopt($this->curlObject,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curlObject, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($this->curlObject, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($this->curlObject, CURLOPT_POST, true);
        curl_setopt($this->curlObject, CURLOPT_POSTFIELDS, $request);
        return json_decode(curl_exec($this->curlObject));
    }
    public function execc($request) {
        $res = $this->exec($request);
        curl_close($this->curlObject);
        return $res;
    }
}