<?php
/**
 * Created by PhpStorm.
 * User: sappy
 * Date: 20.06.17
 * Time: 16:32
 */

interface CryptoInterface {
    public function getBlockHeight();
    public function createNewWallet();
    public function transfer($target,$amount, $args = null);
    public function getBalance($coin_id);
}