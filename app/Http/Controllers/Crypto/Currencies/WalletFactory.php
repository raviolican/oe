<?php
/**
 * Created by PhpStorm.
 * User: sappy
 * Date: 20.06.17
 * Time: 18:32
 */
require_once ('CryptoNote.php');
class WalletFactory {
    private $obj = null;
    public function __construct($data)
    {
        switch ($data) {
            case "oecoin":
                $this->obj = new CryptoNote();
                break;
            case "ethereum":
                $this->obj = null;
                break;
        }
    }
    public function getObject() {
        return $this->obj;
    }
}