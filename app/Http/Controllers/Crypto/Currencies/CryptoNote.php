<?php
require_once ('CryptoInterface.php');
/**
 * Created by PhpStorm.
 * User: sappy
 * Date: 20.06.17
 * Time: 16:34
 */

class CryptoNote implements CryptoInterface {
    protected $coin_name = "OECOIN";
    protected $dirCode   = "\"cd ~/Documents/oecoin/build/release/src; ";

    public function getBlockHeight()
    {
        $rpcHandler = new JSONRPCHandler("127.0.0.1", 89);
        $res = $rpcHandler->exec(
            "{\"params\":{},\"jsonrpc\":\"2.0\",\"id\":\"test\",\"method\":\"get_height\"}");
        // TODO: Implement getBlockHeight() method.

    }
    public function createNewWallet()
    {
        $mySeed = md5(str_shuffle((string)random_int(rand(1,20000),rand(20001,20000000))));
        $fileName = md5(str_shuffle((string)random_int(rand(200,20000000),rand(20000002,50000000))));

        $out = shell_exec("printf 'balance\nexit' | ~/./simplewallet --generate-new-wallet $fileName --password $mySeed");


        $pos        = strpos($out, ":");
        $pos2       = strpos($out, "\n", $pos);
        $address    = substr($out, $pos+2,$pos2-$pos-2);

        $pos        = strpos($out, ":",$pos2 );
        $pos2       = strpos($out,"**");
        $viewkey    = substr($out, $pos+2, $pos2-$pos-3);

        $wallet          = new \App\Wallet();
        $wallet->coin_id = $this->coin_name;
        $wallet->balance = json_encode(array("0","0"));
        $wallet->args    = json_encode(array(
            "address" => $address,
            "password"=> encrypt($mySeed),
            "name" => $fileName,
            "viewKey" => ($viewkey)
        ));

        \Illuminate\Support\Facades\Auth::user()->wallet()->save($wallet);
    }
    public function getBalance($coin_id)
    {
        $user           = \Illuminate\Support\Facades\Auth::user();
        $wallet         = $user->wallet()->where("coin_id", "=", $coin_id);
        $wallet_info    = $wallet->get()->first();
        /*$wallet_info    = Cache::remember(Auth::user()->name.$coin_id, 5, function() use ($wallet) {
                return $wallet->get()->first();
        });*/
        $args           = json_decode($wallet_info->args);
        $data           = Cache::remember($args->name, 2, function() use ($args) {
                return shell_exec("printf 'balance\nexit' | ~/./simplewallet --wallet-file $args->name --password ".decrypt($args->password));

        });
        //var_dump($data);
        $pos    = strpos($data, "balance");
        $pos2   = strpos($data, "\n", $pos);
        $info   = substr($data,$pos,$pos2-$pos);
        $pos    = strpos($info, "balance: ") + strpos($info, " ");
        $pos2   = strpos($info,",");
        $actual = (substr($info,$pos,$pos2-$pos));

        $pos    = strpos($info, " ", $pos2);
        $data   = substr($info, $pos);
        $pos    = strpos($data, ":")+1;
        $pos2   = strlen($data);
        $hold   = (substr($data, $pos, $pos2-$pos));

        $balance = json_encode(array(
            $actual, $hold
        ));

        $wallet_info->balance = $balance;
        $wallet_info->save();

        return $balance;

    }

    public function transfer($target, $amount, $args = null)
    {
        $user   = \Illuminate\Support\Facades\Auth::user();
        $wallet = $user->wallet()->where("coin_id","=", "oecoin")->get()->first();
        $args   = json_decode($wallet->args);

        if((float)json_decode($wallet->balance)[0] < (float)$amount) { // Not Enought Money!
            echo"Nicht Genügend Geldmittel. Vorgang wird abgebrochen";
            return false;
        } else {
            // Transfer Coins!
            $result = shell_exec(" printf 'transfer 0 ".$target." ".$amount."\nexit' | ~/./simplewallet --wallet-file ".$args->name." --password ".decrypt($args->password));

            $pos  = strpos($result, "transaction");
            $pos  = strpos($result, " ", $pos);
            $pos2 = strpos($result, "\n", $pos);

            return substr($result, $pos+1, $pos2-$pos);
        }
        echo "false";

    }
}