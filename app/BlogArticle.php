<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogArticle extends Model
{
    protected $fillable = array(
        "title","slug", "author", "body","keywords", "description", "imageFile"
    );
    //
    public function user() {
        $this->hasOne("\App\User");
    }
}
